package com.punicapp.facebook;


import android.app.Activity;
import android.content.Intent;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.punicapp.sncore.ISNConnector;
import com.punicapp.sncore.SNAccessToken;
import com.punicapp.sncore.SNType;

import java.util.Collections;

import io.reactivex.Observer;
import io.reactivex.subjects.PublishSubject;

public class FacebookSNConnector implements ISNConnector {
    public static final String PUBLIC_PROFILE = "public_profile";
    private Observer<SNAccessToken> observer;
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            observer.onNext(new SNAccessToken(loginResult.getAccessToken().getToken(), null, loginResult.getAccessToken().getUserId()));
            observer.onComplete();
        }

        @Override
        public void onCancel() {
            observer.onError(new Throwable("Cancel"));
            observer.onComplete();

        }

        @Override
        public void onError(FacebookException e) {
            observer.onError(e);
            observer.onComplete();
        }
    };
    private CallbackManager callbackManager;

    @Override
    public SNType getType() {
        return SNType.FB;
    }

    protected boolean isLoggedIn() {
        AccessToken currentAccessToken = AccessToken.getCurrentAccessToken();
        return currentAccessToken != null;
    }

    @Override
    public boolean requestAuth(Activity shadowActivity) {
        if (isLoggedIn()) {
            AccessToken currentAccessToken = AccessToken.getCurrentAccessToken();
            observer.onNext(new SNAccessToken(currentAccessToken.getToken(), null, currentAccessToken.getUserId()));
            observer.onComplete();
            return true;
        }

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, callback);
        LoginManager.getInstance().logInWithReadPermissions(shadowActivity, Collections.singletonList(PUBLIC_PROFILE));
        return false;
    }

    @Override
    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void bindAuthCallback(PublishSubject<SNAccessToken> observer) {
        this.observer = observer;
    }
}
