package com.punicapp.snfacebook;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.punicapp.facebook.FacebookSNConnector;
import com.punicapp.sncore.SNAccessToken;
import com.punicapp.sncore.SNManager;
import com.punicapp.sncore.SNType;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DefaultObserver;

public class MainActivity extends AppCompatActivity {

    private SNManager sManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SNManager.Builder builder = new SNManager.Builder(this);
        printHashKey();
        sManager = builder.with(new FacebookSNConnector())
                .build();

        findViewById(R.id.login_fb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Observable<SNAccessToken> auth = sManager.auth(SNType.FB);
                auth.subscribe(new DefaultObserver<SNAccessToken>() {

                    @Override
                    public void onNext(@NonNull SNAccessToken snAccessToken) {
                        showDialog(snAccessToken.toString());
                        cancel();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        showDialog("ERRROR: " + e.getMessage());
                        cancel();
                    }

                    @Override
                    public void onComplete() {
                        cancel();
                    }
                });
            }
        });


    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(message)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("WAT", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("WAT", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("WAT", "printHashKey()", e);
        }
    }

}
