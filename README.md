# gradle-simple

[![](https://jitpack.io/v/org.bitbucket.punicappinternal/snFacebook.svg)](https://jitpack.io/#org.bitbucket.punicappinternal/snFacebook)

Punicapp facebook module library, which is used in internal projects.

To install the library add: 
 
```
   allprojects {
       repositories {
           maven { url 'https://jitpack.io' }
       }
   }
   dependencies {
           compile 'org.bitbucket.punicappinternal:snFacebook:1.0.0'
   }
```